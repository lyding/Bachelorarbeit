Dieses Verzeichnis beinhaltet eine Implementierung der 
FDTD1D in Python. Die Quelle ist eine sinusfoermige
Quelle mit 1.5e6Hz. Die offensichtliche Assymetry
der Feldstaerkewerte auf der linken und rechten
Seite der Quelle liegt an der Dirichlet Boundary
Condition, die fuer die linke Seite einen
perfekten magnetischen und fuer die rechte Seite
einen perfekten elektrischen Leiter vorgibt.
Platziert man einen perfekten elektrischen Leiter
an Stelle 1 des Universums, so ist die resultierende
Feldstaerke symmetrisch.
